package by.seduce.mvcsecurity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MvcSecurityApplication

fun main(args: Array<String>) {
    runApplication<MvcSecurityApplication>(*args)
}
